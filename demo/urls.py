from django.conf.urls import url,include
from django.contrib import admin

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url(r'^' , include('apps.home.urls')),
    url(r'^person/' , include('apps.person.urls')),
]
