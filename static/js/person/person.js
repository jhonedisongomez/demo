// using jQuery
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {

        var cookies = document.cookie.split(';');

        for (var i = 0; i < cookies.length; i++) {

            var cookie = jQuery.trim(cookies[i]);

            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {

                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


function guardar()
{
	var name = document.getElementById("name").value;
	var lastName = document.getElementById("last_name").value;
	var documentNumber = document.getElementById("document_number").value;
	var cfrstoken = getCookie('csrftoken');

	$.ajax({

		data: {'name': name, 'last_name': lastName, 'document_number': documentNumber, 'csrfmiddlewaretoken': cfrstoken},
		type: 'post',
		url: '/person/createPerson/',

		success: function(response)
		{
			var isSave= response.is_save;

			if(isSave)
			{
				document.getElementById("person_form").reset();
				var message = response.message;
				alert(message);

			}else
			{
				alert("no se pudo guardar")
			}
		}

	});

}