from django.shortcuts import render,render_to_response
from django.views.generic import TemplateView
from django.template import RequestContext
import json
from .models import Person
from django.http import HttpResponse
from braces.views import LoginRequiredMixin
from .forms import CreatePersonForm

class CreatePersonView(LoginRequiredMixin,TemplateView):

    template_name = "person/person_form.html"
    form_class = CreatePersonForm()
    login_url = "/"

    def get(self,request,*args,**kwargs):

        return render_to_response(self.template_name, {'form':self.form_class}, context_instance=RequestContext(request))

    def post(self,request,*args,**kwargs):

        try:

            response_data = {}
            name = request.POST['name']
            last_name = request.POST['last_name']
            document_number = request.POST['document_number']

            person = Person()
            person.name = name
            person.last_name = last_name
            person.document_number = document_number
            person.save()

            response_data['is_save'] = True
            response_data['message'] = "la persona se guardo en la base de datos"

        except Exception as e:

            response_data['is_save'] = False
            response_data['message'] = e.message

        return HttpResponse(json.dumps(response_data), content_type='application/json')            