from django.conf.urls import url,include
from .views import CreatePersonView

urlpatterns = [

    url(r'^createPerson/$', CreatePersonView.as_view(), name='create_person'),
	
]


