# -*- coding: utf-8 -*-
from django import forms
from .models import Person

class CreatePersonForm(forms.ModelForm):

	name = forms.CharField(widget= forms.TextInput(attrs={

		'class' : 'form-control input-md',
		'placeholder' : '',
		'id' : 'name',

	}))

	last_name = forms.CharField(widget= forms.TextInput(attrs={

		'class' : 'form-control input-md',
		'placeholder' : '',
		'id' : 'last_name',


	}))

	document_number = forms.CharField(widget= forms.TextInput(attrs={

		'class' : 'form-control input-md',
		'placeholder' : '',
		'id' : 'document_number',


	}))
	
	class Meta:
		model = Person
		fields = '__all__'
